# Tank Battle V1.0 #

You have been tasked with making a mini game 'tank battle' which will be one of many mini
games in a product. The art assets and basic scene have been supplied by the art department
and you must make a playable prototype.

You should use any coding practices and game play mechanisms you feel are appropriate.
The design specification is as follows

* There is a flat game world with obstacles placed on it.
* The tanks start from two fixed locations.
* One tank (computer) will move to a random position on the map.
* The player will click a location and the other tank will go to the clicked location.
* When both tanks are at their locations they will take turns firing at each other.
* Player can control the elevation and shot power of the gun for each shot. The
computer will control the other one.
* The computer must iteratively find the correct elevation and power.
* When a shell hits its target there will be an explosion and the winner displayed
* All shells must be simulated not calculated