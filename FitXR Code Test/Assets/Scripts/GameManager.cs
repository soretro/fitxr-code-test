﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameState
{
    MainMenu,
    Wait,

    PlayersTurnClickGround,
    PlayersTurnChooseVelocity,
    PlayersTurnShooting,
    PlayerTurnBulletImpact,

    CpusTurnClickGround,
    CpusTurnChooseVelocity,
    CpusTurnShooting,
    CpusTurnBulletImpact,

    GameOverMenu,
}

public class GameManager : MonoBehaviour
{
    [SerializeField] public GameState currentState = GameState.MainMenu;
    [NonSerialized] public PlayerController playerOneController;
    [NonSerialized] public ComputerController playerTwoController;

    private Canvas _mainUiCanvas;
    private Canvas _mainMenuCanvas;
    private Canvas _ingameMessageCanvas;
    private Canvas _gameOverCanvas;

    private Slider _playerOneHeathBar;
    [NonSerialized] public float playerOneHealth = 100;

    private Slider _playerTwoHeathBar;
    [NonSerialized] public float playerTwoHealth = 100;

    private TextMeshProUGUI _startButtonText;
    private TextMeshProUGUI _mainTitleText;
    private TextMeshProUGUI _ingameText;
    private TextMeshProUGUI _finalMessage;

    private Transform _moonLightDirection;

    private void Awake()
    {
        AssignAllDependencies();
    }

    private void Start()
    {
        _mainMenuCanvas.enabled = true;
        _mainUiCanvas.enabled = false;
        _ingameMessageCanvas.enabled = false;
        _gameOverCanvas.enabled = false;

        _playerOneHeathBar.value = _playerOneHeathBar.maxValue;
        _playerTwoHeathBar.value = _playerTwoHeathBar.maxValue;
    }

    private void Update()
    {
        _moonLightDirection.Rotate(Vector3.up, 0.05f);

        switch (currentState)
        {
            case GameState.MainMenu:
                _startButtonText.fontSize = 20f + Mathf.Cos(Time.time * 3f) * 2f;
                _mainTitleText.fontSize = 80f - Mathf.Cos(Time.time * 3f) * 2f;
                _mainTitleText.characterSpacing = 10f - Mathf.Cos(Time.time * 3f) * 2f;
                break;

            // Player States
            case GameState.PlayersTurnClickGround:
                playerOneController.PlayersTurnClickGround();
                break;
            case GameState.PlayersTurnChooseVelocity:
                playerOneController.PlayersTurnChooseVelocity();
                break;
            case GameState.PlayersTurnShooting:
                playerOneController.PlayersTurnShooting();
                break;
            case GameState.PlayerTurnBulletImpact:
                playerOneController.PlayerTurnBulletImpact();
                break;

            // CPU States
            case GameState.CpusTurnClickGround:
                currentState = GameState.Wait;
                playerTwoController.Invoke(methodName:
                    nameof(playerTwoController.CpusTurnClickGround), 1f);
                break;
            case GameState.CpusTurnChooseVelocity:
                currentState = GameState.Wait;
                playerTwoController.Invoke(
                    nameof(playerTwoController.CpusTurnChooseVelocity), 2f);
                break;
            case GameState.CpusTurnShooting:
                playerTwoController.CpusTurnShooting();
                break;
            case GameState.CpusTurnBulletImpact:
                playerTwoController.CpusTurnBulletImpact();
                break;

            case GameState.GameOverMenu:
                break;
        }
    }

    private void AssignAllDependencies()
    {
        _mainUiCanvas = GameObject.Find("Main UI Canvas").GetComponent<Canvas>();
        _mainMenuCanvas = GameObject.Find("Main Menu Canvas").GetComponent<Canvas>();
        _ingameMessageCanvas = GameObject.Find("Ingame Message Canvas").GetComponent<Canvas>();
        _gameOverCanvas = GameObject.Find("Game Over Canvas").GetComponent<Canvas>();

        _playerOneHeathBar = GameObject.Find("Player 1 Health Meter").GetComponent<Slider>();
        _playerTwoHeathBar = GameObject.Find("Player 2 Health Meter").GetComponent<Slider>();

        playerOneController = GameObject.Find("Player1").GetComponent<PlayerController>();
        playerTwoController = GameObject.Find("Player2").GetComponent<ComputerController>();

        _startButtonText = GameObject.Find("Start Button Text").GetComponent<TextMeshProUGUI>();
        _mainTitleText = GameObject.Find("Game Title").GetComponent<TextMeshProUGUI>();
        _ingameText = GameObject.Find("Ingame Text").GetComponent<TextMeshProUGUI>();
        _finalMessage = GameObject.Find("Final Message").GetComponent<TextMeshProUGUI>();

        _moonLightDirection = GameObject.Find("Moon Rig").transform;
    }

    public void StartGame()
    {
        _mainMenuCanvas.enabled = false;
        _mainUiCanvas.enabled = true;
        _ingameMessageCanvas.enabled = false;
        _gameOverCanvas.enabled = false;

        currentState = GameState.PlayersTurnClickGround;
    }

    public void Player1Hit(float damage)
    {
        playerOneHealth -= damage;
        _playerOneHeathBar.value = playerOneHealth;

        ShowDamageMessage(damage);

        if (playerOneHealth <= 0)
        {
            PrepareForGameOverScreen();
        }
    }

    public void Player2Hit(float damage)
    {
        playerTwoHealth -= damage;
        _playerTwoHeathBar.value = playerTwoHealth;

        ShowDamageMessage(damage);

        if (playerTwoHealth <= 0)
        {
            PrepareForGameOverScreen();
        }
    }

    private void PrepareForGameOverScreen()
    {
        currentState = GameState.GameOverMenu;
        CancelInvoke();
        playerOneController.CancelInvoke();
        playerTwoController.CancelInvoke();
        Invoke(nameof(GameOver), 2f);
    }

    private void ShowDamageMessage(float damage)
    {
        switch (damage)
        {
            case 20f:
                SendIngameMessage("Direct Hit!");
                break;
            case 10f:
                SendIngameMessage("10% Damage");
                break;
            case 5f:
                SendIngameMessage("5% Damage");
                break;
        }
    }

    private void GameOver()
    {
        if (playerOneHealth <= 0)
        {
            _finalMessage.text = "You Lose";
        }
        else
        {
            _finalMessage.text = "You Win";
        }

        _gameOverCanvas.enabled = true;
        _ingameMessageCanvas.enabled = false;
    }

    public string SendIngameMessage(string message)
    {
        _ingameText.text = message;
        _ingameMessageCanvas.enabled = true;
        _gameOverCanvas.enabled = false;
        Invoke(nameof(HideImgameMessage), 1f);

        return null;
    }

    private void HideImgameMessage()
    {
        _ingameMessageCanvas.enabled = false;
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Scenes/TankBattle");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}