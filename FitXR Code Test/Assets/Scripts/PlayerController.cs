﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : PlayerControllerBase
{
    [SerializeField] private Material groundMaterial;

    private GameObject _enemy;

    private float _firePressedDuration;
    private bool _fireButtonActive;
    private float _firePowerScale = 8f;

    private static readonly int Center = Shader.PropertyToID("_Center");
    private static readonly int AreaColor = Shader.PropertyToID("_AreaColor");

    protected new void Awake()
    {
        base.Awake();

        _enemy = GameObject.Find("Player2");
    }

    private new void Start()
    {
        base.Start();

        SetupSliderEvents();

        bullet.enemy = _enemy.GetComponent<ComputerController>();
    }

    public void PlayersTurnClickGround()
    {
        // gameManager.SendIngameMessage("Click to move tank");

        var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        var intersectsCollider = Physics.Raycast(ray.origin, ray.direction, out raycastHit);
        var hitGround = raycastHit.collider.name == ground.name;

        if (intersectsCollider)
        {
            groundMaterial.SetColor(AreaColor, hitGround ? Color.red : Color.white);
        }

        groundMaterial.SetVector(Center, raycastHit.point);

        if (Input.GetMouseButtonUp(0))
        {
            if (uiWasNotClicked && userClickedGround)
            {
                navMeshAgent.destination = raycastHit.point;
                gameManager.currentState = GameState.Wait;

                StartCoroutine(WaitUntilAtDestination());
            }
        }
    }

    private IEnumerator WaitUntilAtDestination()
    {
        while (Vector3.Distance(transform.position, navMeshAgent.destination) > 2f)
        {
            yield return new WaitForSeconds(0.5f);
        }

        gameManager.currentState = GameState.CpusTurnClickGround;
        yield return 0;
    }

    public void PlayersTurnChooseVelocity()
    {
        if (!navMeshAgent.velocity.Equals(Vector3.zero)) return;

        // gameManager.SendIngameMessage("Choose angle and fire");

        if (Input.GetMouseButton(0) && _fireButtonActive && !bullet.isFiring)
        {
            _firePressedDuration += Time.deltaTime;

            bullet.power = _firePressedDuration * _firePowerScale;
            powerSlider.value = _firePressedDuration;

            if (_firePressedDuration >= firePressedMaxDuration)
            {
                _firePressedDuration = firePressedMaxDuration;
                gameManager.currentState = GameState.PlayersTurnShooting;
                bullet.power = _firePressedDuration * _firePowerScale;
                FireBullet();
                _firePressedDuration = 0;
                _fireButtonActive = false;
            }

            powerTitleText.text = "Power: " + bullet.power.ToString("F1");
        }

        if (Input.GetMouseButtonDown(0) && !bullet.isFiring)
        {
            _fireButtonActive = true;
        }

        if (Input.GetMouseButtonUp(0) && _fireButtonActive && !bullet.isFiring)
        {
            gameManager.currentState = GameState.PlayersTurnShooting;

            _fireButtonActive = false;
            bullet.power = _firePressedDuration * _firePowerScale;
            FireBullet();
            _firePressedDuration = 0;
        }

        if (Input.GetKey(KeyCode.A))
        {
            TiltTurret(-1);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            TiltTurret(1);
        }

        if (Input.GetKey(KeyCode.W))
        {
            TurnTurret(1);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            TurnTurret(-1);
        }
    }

    public void PlayersTurnShooting()
    {
    }

    public void PlayerTurnBulletImpact()
    {
        var bulletPos = bullet.transform.position;
        var player2Pos = gameManager.playerTwoController.transform.position;

        var distance = Vector3.Distance(bulletPos, player2Pos);

        if (bullet.directHit)
        {
            gameManager.Player2Hit(20f);
        }
        else if (distance < 1f)
        {
            gameManager.Player2Hit(10f);
        }
        else if (distance < 3f)
        {
            gameManager.Player2Hit(5f);
        }
        else
        {
            gameManager.SendIngameMessage("MISSED!");
        }

        ComputersTurnDelay();
    }

    private void ComputersTurnDelay()
    {
        CancelInvoke();
        if (gameManager.playerTwoHealth > 0)
        {
            gameManager.currentState = GameState.CpusTurnChooseVelocity;
        }
    }

    private void SetupSliderEvents()
    {
        angleSlider = GameObject.Find("Angle Slider").GetComponent<Slider>();
        angleSlider.onValueChanged.AddListener(delegate { ValueChangeAngle(); });

        powerSlider = GameObject.Find("Power Slider").GetComponent<Slider>();
        powerSlider.onValueChanged.AddListener(delegate { ValueChangePower(); });

        firePressedMaxDuration = powerSlider.maxValue;
    }

    private void ValueChangeAngle()
    {
        bullet.angle = angleSlider.value;

        var newAngle = tower.transform.rotation.eulerAngles;
        newAngle.x = bullet.angle;
        tower.transform.rotation = Quaternion.Euler(newAngle);
    }

    private void ValueChangePower()
    {
        bullet.power = powerSlider.value;
        fireMeter.value = bullet.power;
    }
}