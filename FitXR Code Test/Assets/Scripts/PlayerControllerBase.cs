﻿using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerControllerBase : MonoBehaviour
{
    [SerializeField] private Color color;

    protected GameManager gameManager;

    protected Camera mainCamera;
    private Transform _canvas;

    protected float firePressedMaxDuration;

    private const float TURRET_ANGLE_MIN = -90f;
    private const float TURRET_ANGLE_MAX = 0f;

    private GameObject _bullet;
    private GameObject _bulletPrefab;
    protected GameObject bulletStartPosition;
    protected BulletController bullet;

    private Transform _tankRoot;
    private MeshRenderer _towerMeshRenderer;
    private MeshRenderer _bodyMeshRenderer;

    protected Transform tower;
    protected Transform towerController;

    protected Slider fireMeter;

    protected NavMeshAgent navMeshAgent;
    protected RaycastHit raycastHit;
    protected GameObject ground;

    protected Slider angleSlider;
    protected Slider powerSlider;

    private TextMeshProUGUI _angleTitleTmp;
    protected TextMeshProUGUI powerTitleText;

    protected void Awake()
    {
        AssignAllDependencies();
    }

    protected void Start()
    {
        CreateBullet();
        bullet = _bullet.GetComponent<BulletController>();
        bullet.angle = TURRET_ANGLE_MIN;
        bullet.power = 5f;
    }

    private void AssignAllDependencies()
    {
        mainCamera = Camera.main;

        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();

        _tankRoot = transform.Find("Tank Root");
        towerController = _tankRoot.Find("TowerController");
        tower = towerController.Find("Tower");
        _canvas = _tankRoot.Find("Canvas");

        var canvasComp = _tankRoot.Find("Canvas").GetComponentInChildren<Canvas>();
        canvasComp.worldCamera = mainCamera;

        _bodyMeshRenderer = _tankRoot.Find("Body").GetComponentInChildren<MeshRenderer>();
        _bodyMeshRenderer.material.color = color;

        _towerMeshRenderer = tower.GetComponentInChildren<MeshRenderer>();
        _towerMeshRenderer.material.color = color;

        bulletStartPosition = tower.Find("Bullet Start Position").gameObject;

        fireMeter = _canvas.Find("Fire Meter").GetComponent<Slider>();

        navMeshAgent = GetComponent<NavMeshAgent>();
        raycastHit = new RaycastHit();
        ground = GameObject.Find("Ground");

        _angleTitleTmp = GameObject.Find("Angle Title").GetComponent<TextMeshProUGUI>();
        powerTitleText = GameObject.Find("Power Title").GetComponent<TextMeshProUGUI>();
    }

    private void CreateBullet()
    {
        _bulletPrefab = Resources.Load<GameObject>("TankBulletPrefab");
        var bulletPosition = bulletStartPosition.transform.position;
        var bulletRotation = bulletStartPosition.transform.rotation;
        _bullet = Instantiate(_bulletPrefab, bulletPosition, bulletRotation);
    }

    protected void FireBullet(Vector3 velocity = default)
    {
        if (velocity == default)
        {
            bullet.Fire(bulletStartPosition);
        }
        else
        {
            bullet.Fire(bulletStartPosition, velocity);
        }
    }

    protected void ResetBullet()
    {
        var tf = bullet.transform;
        tf.position = bulletStartPosition.transform.position;
        tf.rotation = bulletStartPosition.transform.rotation;
    }

    protected void TiltTurret(float direction)
    {
        var newAngle = transform.localRotation.eulerAngles;
        newAngle.y += 0.5f * direction * Time.deltaTime * 100f;
        transform.localRotation = Quaternion.Euler(newAngle);
    }

    protected void TurnTurret(float direction)
    {
        bullet.angle += 0.5f * direction * Time.deltaTime * 100f;
        bullet.angle = Mathf.Clamp(bullet.angle, TURRET_ANGLE_MIN, TURRET_ANGLE_MAX);
        angleSlider.value = bullet.angle;
        var newAngle = towerController.transform.rotation.eulerAngles;
        newAngle.x = bullet.angle;
        towerController.transform.rotation = Quaternion.Euler(newAngle);

        _angleTitleTmp.text = "Angle: " + (bullet.angle + 90f).ToString("F1") + "º";
    }

    protected static bool uiWasNotClicked => !EventSystem.current.IsPointerOverGameObject();

    protected bool userClickedGround
    {
        get
        {
            var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            var intersectsCollider = Physics.Raycast(ray.origin, ray.direction, out raycastHit);
            var hitGround = raycastHit.collider.name == ground.name;

            return intersectsCollider && hitGround;
        }
    }
}