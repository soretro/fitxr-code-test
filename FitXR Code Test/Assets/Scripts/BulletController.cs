﻿using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] private AudioSource gunFireAudioClip;
    [SerializeField] private AudioSource impactAudioClip;
    [SerializeField] public Rigidbody _rigidbody;
    [SerializeField] private ParticleSystem explosionParticleSystem;

    public float power;
    public float angle;

    private GameManager _gameManager;

    private Vector3 _startPosition;
    private Quaternion _startRotation;
    private MeshRenderer _meshRenderer;
    private bool _isExploding;

    public PlayerControllerBase enemy;
    public bool isFiring;
    public bool directHit;

    public string lastObjectHit;

    // private Fun

    private void Awake()
    {
        AssignAllDependencies();

        isFiring = false;
        _isExploding = false;
    }

    private void Update()
    {
        if (transform.position.y < -10)
        {
            _isExploding = true;
            KillBullet();
            CancelInvoke();

            if (enemy.name == "Player1")
            {
                _gameManager.currentState = GameState.CpusTurnBulletImpact;
            }
            else
            {
                _gameManager.currentState = GameState.PlayerTurnBulletImpact;
            }
        }
    }

    private void AssignAllDependencies()
    {
        _gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();

        gunFireAudioClip = GetComponents<AudioSource>()[0];
        impactAudioClip = GetComponents<AudioSource>()[1];

        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.Sleep();

        _meshRenderer = gameObject.GetComponent<MeshRenderer>();
        _meshRenderer.enabled = false;
    }

    public void Fire(GameObject startPosition, Vector3 velocity = default)
    {
        if (isFiring) return;

        _startPosition = startPosition.transform.position;
        _startRotation = startPosition.transform.rotation;

        var tempTransform = transform;
        tempTransform.position = _startPosition;
        tempTransform.rotation = _startRotation;

        _rigidbody.isKinematic = false;
        _meshRenderer.enabled = true;
        isFiring = true;

        _rigidbody.velocity = Vector3.zero;
        _rigidbody.WakeUp();

        if (velocity == default)
        {
            _rigidbody.velocity = _startRotation * Vector3.forward * power;
        }
        else
        {
            _rigidbody.velocity = velocity;
        }


        gunFireAudioClip.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Ground" ||
            other.name == "Far Ground" ||
            other.name.Contains("Rock") ||
            other.name.Contains("Wall") ||
            other.name == enemy.name)
        {
            lastObjectHit = other.name;
            _isExploding = true;
            CancelInvoke();
            KillBullet();
        }

        if (_isExploding)
        {
            if (enemy.name == "Player1")
            {
                _gameManager.currentState = GameState.CpusTurnBulletImpact;
            }
            else
            {
                _gameManager.currentState = GameState.PlayerTurnBulletImpact;
            }

            directHit = other.name == enemy.name ? true : false;
        }
    }

    private void KillBullet()
    {
        impactAudioClip.Play();
        DisableBullet();

        explosionParticleSystem.Play();
        Invoke(nameof(Reset), 1f);
    }

    private void DisableBullet()
    {
        _rigidbody.isKinematic = true;
        _rigidbody.velocity = Vector3.zero;
        _meshRenderer.enabled = false;
    }

    private void Reset()
    {
        _rigidbody.Sleep();
        isFiring = false;
        _isExploding = false;
    }
}