﻿using System.Collections;
using UnityEngine;

public class ComputerController : PlayerControllerBase
{
    private GameObject _enemy;
    private GameObject[] _spawnPoints;

    private bool _isSpawnSet;
    private int _randSpawn;

    private float _shootTime = 3f;
    private float _shootTimeMultiplier = 1f;
    private float _accuracyLevel;

    protected new void Awake()
    {
        base.Awake();

        _enemy = GameObject.Find("Player1");
        _spawnPoints = GameObject.FindGameObjectsWithTag("CPU Spawn Point");
    }

    protected new void Start()
    {
        base.Start();

        bullet.enemy = _enemy.GetComponent<PlayerController>();
        _accuracyLevel = Random.Range(6f, 10f);
    }

    public string CpusTurnClickGround()
    {
        if (_isSpawnSet) return null;
        _isSpawnSet = true;

        _randSpawn = Random.Range(0, _spawnPoints.Length);
        // _randSpawn = 0; // For debugging only
        navMeshAgent.destination = _spawnPoints[_randSpawn].transform.position;

        StartCoroutine(WaitUntilAtDestination());

        return null;
    }

    private IEnumerator WaitUntilAtDestination()
    {
        while (Vector3.Distance(transform.position, _spawnPoints[_randSpawn].transform.position) > 1f)
        {
            yield return null;
        }

        gameManager.currentState = GameState.PlayersTurnChooseVelocity;
        yield return 0;
    }

    public string CpusTurnChooseVelocity()
    {
        gameManager.currentState = GameState.CpusTurnShooting;

        var enemyPos = _enemy.transform.position;

        _shootTime = Vector3.Distance(enemyPos, transform.position) / 10f;

        if (bullet.lastObjectHit.Contains("Rock"))
        {
            _shootTimeMultiplier *= 1.2f;
        }

        _shootTime *= _shootTimeMultiplier;

        Vector3 accLev = new Vector3(
            Random.Range(0, _accuracyLevel),
            0f,
            Random.Range(0, _accuracyLevel)
        );

        Vector3 shootVelocity = CalculateVelocity(
            enemyPos + accLev,
            bulletStartPosition.transform.position,
            _shootTime
        );

        towerController.rotation = Quaternion.LookRotation(shootVelocity, Vector3.up);

        FireBullet(shootVelocity);

        Invoke(nameof(ShootDelay), _shootTime);

        return null;
    }

    private void ShootDelay()
    {
        gameManager.currentState = GameState.PlayersTurnChooseVelocity;
    }

    public void CpusTurnShooting()
    {
    }

    public void CpusTurnBulletImpact()
    {
        var bulletPos = bullet.transform.position;
        var player1Pos = gameManager.playerOneController.transform.position;

        var distance = Vector3.Distance(bulletPos, player1Pos);

        if (bullet.directHit)
        {
            gameManager.Player1Hit(20f);
        }
        else if (distance < 1f)
        {
            gameManager.Player1Hit(10f);
        }
        else if (distance < 3f)
        {
            gameManager.Player1Hit(5f);
        }
        else
        {
            gameManager.SendIngameMessage("MISSED!");
        }

        PlayersTurnDelay();
    }

    private void PlayersTurnDelay()
    {
        _accuracyLevel *= 0.8f;
        if (_accuracyLevel < 0) _accuracyLevel = 0;
        CancelInvoke();
        if (gameManager.playerOneHealth > 0)
        {
            gameManager.currentState = GameState.PlayersTurnChooseVelocity;
        }

        _isSpawnSet = false;
    }

    private Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
    {
        Vector3 distance = target - origin;
        Vector3 distanceXz = distance;
        distanceXz.y = 0f;

        float sy = distance.y;
        float sxz = distanceXz.magnitude;

        float vxz = sxz / time;
        float vy = sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = distanceXz.normalized;
        result *= vxz;
        result.y = vy;

        return result;
    }
}